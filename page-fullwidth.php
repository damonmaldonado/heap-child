<?php
/**
 * Template Name: Full Width
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 */

get_header();

global $wpgrade_private_post;

if ( post_password_required() && !$wpgrade_private_post['allowed'] ) {
	// password protection
    get_template_part('theme-partials/password-request-form');

} else { ?>
    <div class="page-content single-page-content">
            <?php if ( is_active_sidebar( 'pre-content' ) ) : ?>
    <ul class="pre-content">
        <?php dynamic_sidebar( 'pre-content' ); ?>
    </ul>
    <?php endif; ?>

        <?php if (have_posts()): the_post(); ?>

            <article class="article page page-single page-regular">
                <header>
                    <?php if (has_post_thumbnail()):
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full-size');
					if (!empty($image[0])): ?>
                    <div class="page__featured-image">
                        <img src="<?php echo $image[0] ?>" alt="<?php the_title(); ?>"/>
                    </div>
                    <?php endif;
					endif;?>
                </header>
                <div class="page__wrapper">
                    <section class="page__content  js-post-gallery  cf">
						<?php //if (wpgrade::option('blog_single_show_breadcrumb')) heap::the_breadcrumb(); ?>
                        <h1 class="page__title"><?php the_title(); ?></h1>
                        <hr class="separator separator--dark" />
                                            <?php if ( wpgrade::option( 'blog_single_show_share_links' ) && ( wpgrade::option( 'blog_single_share_links_position', 'bottom' ) == 'top' || wpgrade::option( 'blog_single_share_links_position', 'bottom' ) == 'both' ) ): ?>
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style  add_this_list"
                             addthis:url="<?php echo wpgrade_get_current_canonical_url(); ?>"
                             addthis:title="<?php wp_title( '|', true, 'right' ); ?>"
                             addthis:description="<?php echo trim( strip_tags( get_the_excerpt() ) ) ?>">
                            <?php get_template_part( 'theme-partials/wpgrade-partials/addthis-social-buttons' ); ?>
                        </div>
                    <?php endif; ?>
                        <?php the_content(); ?>

                    </section>
                    <?php edit_post_link('Edit');?>
                    <?php 
                    global $numpages; 
                    if($numpages > 1):
                    ?>
                    <div class="entry__meta-box  meta-box--pagination">
                        <span class="meta-box__title"><?php _e('Pages', wpgrade::textdomain()) ?></span>
                        <?php
                        $args = array(
                            'before' => '<ol class="nav  pagination--single">',
                            'after' => '</ol>', 
                            'next_or_number' => 'next_and_number',
                            'previouspagelink' => __('&laquo;', wpgrade::textdomain()),
                            'nextpagelink' => __('&raquo;', wpgrade::textdomain())
                        );
                        wp_link_pages( $args ); 
                        ?>
                    </div>
                    <?php
                    endif;?>
                    <div class="entry__meta-box  meta-box--pagination">
                       <?php $categories = get_the_category();
    if ( !is_wp_error($categories) && !empty( $categories ) ): ?>
        <div class="meta--categories btn-list  meta-list">
            <span class="btn  btn--small  btn--secondary  list-head"><?php _e('Categories', wpgrade::textdomain()) ?></span>
            <?php
            foreach ($categories as $category) {
                echo '<a class="btn  btn--small  btn--tertiary" href="'. get_category_link($category->term_id) .'" title="' . esc_attr(sprintf(__("View all posts in %s", wpgrade::textdomain()), $category->name)) . '" rel="tag">'. $category->name .'</a>';
            }; ?>
        </div>
    <?php endif;

    $tags = get_the_tags();
    if ( !empty( $tags ) ): ?>
        <div class="meta--tags  btn-list  meta-list">
            <span class="btn  btn--small  btn--secondary  list-head"><?php _e('Tags', wpgrade::textdomain()) ?></span>
            <?php
            foreach ($tags as $tag) {
                echo '<a class="btn  btn--small  btn--tertiary" href="'. get_tag_link($tag->term_id) .'" title="'. esc_attr(sprintf(__("View all posts tagged %s", wpgrade::textdomain()), $tag->name)) .'" rel="tag">'. $tag->name .'</a>';
            }; ?>
        </div>
    <?php endif; ?>
    </div>
                            <?php if ( wpgrade::option( 'blog_single_show_share_links' ) && ( wpgrade::option( 'blog_single_share_links_position', 'bottom' ) == 'bottom' || wpgrade::option( 'blog_single_share_links_position', 'bottom' ) == 'both' ) ): ?>
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style  add_this_list"
                             addthis:url="<?php echo wpgrade_get_current_canonical_url(); ?>"
                             addthis:title="<?php wp_title( '|', true, 'right' ); ?>"
                             addthis:description="<?php echo trim( strip_tags( get_the_excerpt() ) ) ?>">
                            <?php get_template_part( 'theme-partials/wpgrade-partials/addthis-social-buttons' ); ?>
                        </div>
                    <?php endif; ?>
    <?php $comm_num= get_comments_number(); 
$comm_num_display = "Comment";
if ($comm_num == 1 ){$comm_num_display = $comm_num . " Comment";}
if ($comm_num > 1 ){$comm_num_display = $comm_num . " Comments";}
?>
            <?php   // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() ) {?>
<div class="omsc-accordion">
<div class="omsc-toggle omsc-in-accordion"><div class="omsc-toggle-title"><?php echo $comm_num_display; ?></div><div class="omsc-toggle-inner" >

<?php comments_template(); ?>
</div></div>
</div>


<?php   } ?>
                </div>
            </article>

        <?php
        else :
            get_template_part( 'no-results' );
        endif; ?>

    </div><!-- .page-content -->

<?php } // close if password protection

get_footer();
