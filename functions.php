<?php
/**
 * If you would like to overwrite the css of the theme you will need to uncomment this action
 */

add_action('wp_enqueue_scripts', 'load_child_theme_styles', 999);

function load_child_theme_styles(){

    // If your css changes are minimal we recommend you to put them in the main style.css.
    // In this case uncomment bellow

    wp_enqueue_style( 'child-theme-style', get_stylesheet_directory_uri() . '/style.css' );

    // If you want to create your own file.css you will need to load it like this (Don't forget to uncomment bellow) :
    //** wp_enqueue_style( 'custom-child-theme-style', get_stylesheet_directory_uri() . '/file.css' );
}
/*add pre-content sidebar*/

/**
 * Register Sidebar
 */
function olv_register_sidebars() {

	/* Register the primary sidebar. */
	register_sidebar(
		array(
			'id' => 'pre-content',
			'name' => __( 'Pre Content Sidebar', 'olv' ),
			'description' => __( 'Displayed Between Nav bar and Content.', 'olv' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s pre-content-widget">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);

	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'olv_register_sidebars' );

class olv_heap
{
static function the_new_archive_title() {

		$object = get_queried_object();

		if ( is_home() ) {
			//do nothing - no title is needed
		} elseif ( is_search() ) { ?>
			<div class="heading headin--main">
				<span class="archive__side-title beta"><?php _e('Search Results for: ', wpgrade::textdomain() ) ?></span>
				<h2 class="hN"><?php echo get_search_query(); ?></h2>
			</div>
			<hr class="separator" />
		<?php
		} elseif ( is_tag() ) { ?>
			<div class="heading headin--main">
				<h2 class="hN"><?php echo single_tag_title( '', false ); ?></h2>
			</div>
			<hr class="separator" />
		<?php } elseif (!empty($object) && isset($object->term_id)) { ?>
			<div class="heading headin--main">
				<h2 class="hN"><?php echo $object->name; ?></h2>
			</div>
			<hr class="separator" />
		<?php } elseif ( is_day() ) {?>
			<div class="heading headin--main">
				<span class="archive__side-title beta"><?php _e('Daily Archives: ', wpgrade::textdomain() ) ?></span>
				<h2 class="hN"><?php echo get_the_date(); ?></h2>
			</div>
			<hr class="separator" />
		<?php } elseif ( is_month() ) { ?>
			<div class="heading headin--main">
				<span class="archive__side-title beta"><?php _e('Monthly Archives: ', wpgrade::textdomain() ) ?></span>
				<h2 class="hN"><?php echo get_the_date( _x( 'F Y', 'monthly archives date format', wpgrade::textdomain() ) ) ; ?></h2>
			</div>
			<hr class="separator" />
		<?php } elseif ( is_year() ) { ?>
			<div class="heading headin--main">
				<span class="archive__side-title beta"><?php _e('Yearly Archives: ', wpgrade::textdomain() ) ?></span>
				<h2 class="hN"><?php echo get_the_date( _x( 'Y', 'yearly archives date format', wpgrade::textdomain() ) ) ; ?></h2>
			</div>
			<hr class="separator" />
		<?php } else { ?>
			<div class="heading headin--main">
			</div>
			<hr class="separator" />
		<?php }

	}
	}

	add_post_type_support( 'page', 'excerpt' );

	function related_default_thumb ($thumb) {
		return '/wp-content/uploads/2015/04/olvlogo.png';
	}


add_filter( 'bawmrp_no_thumb', 'related_default_thumb');	

add_filter( 'bawmrp_li_style', 'related_style');


function related_style($style) {
	return "";
}
