	<?php
/**
 * The Template for displaying all single posts.
 */

get_header();

global $wpgrade_private_post;

if ( post_password_required() && ! $wpgrade_private_post['allowed'] ) {
	// password protection
	get_template_part( 'theme-partials/password-request-form' );

} else {

	if ( have_posts() ) : the_post();

		$has_sidebar = false;
		if ( wpgrade::option( 'blog_single_show_sidebar' ) ) {
			$has_sidebar = true;
		}

		// get_template_part('theme-partials/post-templates/single-content/'. $single_layout . $with_sidebar);

		//post thumb specific
		$has_thumb = has_post_thumbnail();

		$post_class_thumb = 'has-thumbnail';
		if ( ! $has_thumb ) {
			$post_class_thumb = 'no-thumbnail';
		}

		//post format specific
		$post_format = get_post_format();
		if ( empty( $post_format ) || $post_format == 'standard' ) {
			$post_format = '';
		}
		$post_format_class = '';
		if ( ! empty( $post_format ) ) {
			$post_format_class = 'article--' . $post_format;
		}; ?>
		<div class="page-content  single-content<?php if ( $has_sidebar ) {
			echo ' has-sidebar';
		} ?>">
			<?php if ( $has_sidebar ) {
				echo '<div class="page-content__wrapper">';
			} ?>
						<?php if ( is_active_sidebar( 'pre-content' ) ) : ?>
	<ul class="pre-content">
		<?php dynamic_sidebar( 'pre-content' ); ?>
	</ul>
<?php endif; ?>
			<article  <?php post_class( 'article-single  single-post ' . $post_format_class . ' ' . $post_class_thumb ) ?>>
<div id="main-content">
				<?php get_template_part( 'theme-partials/post-templates/single-content/header' ); ?>

				<section class="article__content entry-content js-post-gallery">
					<?php the_content(); ?>
				</section>
				<?php edit_post_link('Edit');?>
				<!-- .article__content -->
				<?php get_template_part( 'theme-partials/post-templates/single-content/footer' );?>
<?php $comm_num= get_comments_number(); 
$comm_num_display = "Comment";
if ($comm_num == 1 ){$comm_num_display = $comm_num . " Comment";}
if ($comm_num > 1 ){$comm_num_display = $comm_num . " Comments";}
?>
			<?php	// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) {?>
<div class="omsc-accordion">
<div class="omsc-toggle omsc-in-accordion"><div class="omsc-toggle-title"><?php echo $comm_num_display; ?></div><div class="omsc-toggle-inner" >

<?php comments_template(); ?>
</div></div>
</div>


<?php	} ?>
</div>

			</article>


			<?php if ( $has_sidebar ) {
				echo '</div><!-- .page-content__wrapper -->';
			} ?>
		</div>
		<?php
		if ( $has_sidebar ) {
			get_template_part( 'sidebar' );
		}
	endif;
}
get_footer();
